# Git

---

## Una breve storia

@ul

- Un file nuovo
- Una copia del file
- Un'altra copia del file
- Un backup vecchio
- Una copia su un'altra macchina
- ...

@ulend

+++

## Un'altra breve storia

@ul

- Stampo e correggo a matita
- Ti mando il file
- L'avevi modificato?
- Qual è l'ultima copia?
- Ma chi ha fatto questa modifica?
- ...

@ulend

---

## Cos'è Git?

@css[fragment](Sistema)
@css[fragment](di Controllo Versione)
@css[fragment](Distribuito)

@css[fragment](_Distributed Version Control System_)

+++

### Un sistema o software

Un tool da usare e integrare nel proprio lavoro

+++

### di controllo versione

Senza non si hanno informazioni
sulla storia di un file
o di una directory

+++

### distribuito o decentralizzato

Ognuno possiede copia del _repository_,
ognuno può lavorare indipendentemente
e alla pari

---

## Perché Git?

Almeno due motivi...

---

# 1.

Per rispondere alle domande:

+++

"Che cosa sto guardando?"

+++

"Su cosa stavo lavorando?"

+++

"Che versione è questa?"

---

# 2.

Per collaborare senza darsi (troppo) fastidio

+++

Modifiche indipendenti

+++

Branch per lavorare in parallelo

+++

Merge (request) per integrare

---


## Caratteristiche

@ul

- Attuale vincitore
- Potente (e complesso)
- Usato (in qualche modo) da tutti
- Astrae il concetto di directory
- Astrae il concetto di modifica

@ulend

---

## GitHub, GitLab, ...

@ul

- server centralizzati (non necessari ma comodi)
- ospitano i repository
- gestiscono gli utenti e i permessi
- interfaccia web
- offrono strumenti e integrazioni
- facilitano la comunicazione (remota)
- sia tra autori cȟe con collaboratori esterni
- (non completamente free)

@ulend

+++

@ul

- **Scoperta**: elenco, navigazione
- **Documentazione**: file, wiki, pages, snippets
- **Debugging**: issue, board
- **Collaborazione**: fork, merge request

@ulend

---

**_[Avanti!](https://gitlab.com/azienda/appunti-su-gitlab/)_**



