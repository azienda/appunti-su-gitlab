---
layout: page
---

# Caratteristiche di GitLab


## Scoperta

- Elenco dei repository (per i nuovi, per chi non è addentro al progetto, per gli anziani)
- Esplorazione "web"
- Ricerca
- Sfogliare repository online
- Clone (no SSH)
- Scoprire chi è "responsabile" (senza essere locali)


## Documentazione

- Semplici file MD in repository (con auto preview)
- Wiki (alternativa a src)
- GitLab Pages (HTML, Jekyll, Hugo, ...): web
- Snippets ("files")


## Gestione Problemi

- Issue
- Board (issue + tag)
- Ref con code (fix #6)
- Design


## Collaborazione

- Infrastruttura simile a GitHub e altri, quindi "nota"
- Atto "formale" ma semplice
- Fork
- Diff
- Merge
- Merge request (?)


## Futuro

- "Standard", comune, simile
- Open source (circa)
- In crescita, momento
- Espandibile: tools, api, integrazioni, etc
