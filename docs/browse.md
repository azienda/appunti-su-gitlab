
# Browsing interface

L'interfaccia web spinge al browsing: esplorare, sfogliare, scoprire.

Questo senza dover chiedere "cosa" e "dove".

Scoprire chi sono i [responsabili](https://gitlab.com/azienda/appunti-su-gitlab/project_members)
e chi ci [lavora](https://gitlab.com/azienda/appunti-su-gitlab/graphs/master).

_**[Avanti!](docs/markdown.md)**_
