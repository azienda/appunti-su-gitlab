---
title: GitLab Pages
layout: page
---

La **terza** possibilità di documentare è data da GitLab Pages.

A differenza di GitHub Pages si può usare qualsiasi _static website generator_.
Potrebbe essere usato anche per documentare _esternamente_, poiché genera un semplice sito.
Potrebbe integrare la documentazione formale (OpenAPI) e quella informale.

Nota: GitLab Pages dipende da CI/CD.

_**[Avanti!](https://gitlab.com/azienda/appunti-su-gitlab/snippets/1807360)**_
