---
layout: page
---

# Prossimi possibili passi


## Fase 1

- Funzionalità di "gatekeeper" per accesso `git+ssh` (chiavi),
`git+http(s)` (credenziali):
risolve problema di utenti ssh e permessi su repository.

- Integrazione LDAP

- Cambio di Git remote, migrazione di un progetto alla volta (ma senza lasciare cose troppo indietro)


## Fase 2
(anche in parallelo con la 1)

- Decisioni progetto per progetto, iniziando con "progetti pilota"

- Uso degli strumenti: tutti quelli disponibili con GitLab o mantenimento di issue e/o wiki esterni

- Stabilire qualche policy: Manutentori/responsabili (un solo gruppo o piu gruppi?) con gruppi; regole; strumenti


## Fase 3

- Uso di strumenti che non abbiamo mai avuto: ad es. CI integrata

- Integrazione con altri tool (Jenkins) con plugin, hook, api, ...

- Policy per l'accettazione di push (via merge request?), dove serve

- Sistemi interni di code review?


