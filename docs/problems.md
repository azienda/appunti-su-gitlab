---
layout: page
---

# Problemi (o Possibilità)


## Non completamente open-source

- Features a pagamento (kanban, merge, enterprise, etc.)


## Tante (troppe?) cose

Vuole fare **tutto**.

- Repository Git "ufficiale" (anche se mirror e no ssh)
- HL Branch e Merge
- Issue (vs Jira o Trello)
- Wiki (vs Confluence): semplice
- CI/CD (vs Jenkins): **risorse**?
  * Testing (vs local?)
  * Pages (vs pdf?)
- Registry: **risorse**?

- In divenire: nuove feature, modifiche, star dietro, sekurity


## Design e Requirements

- Trello
- Confluence


## Filosofia vs Realtà

- Piccoli progetti indipendenti vs monolitiche codebase storiche
- Progetti autonomi vs interdipendenze con progetti/database/macchine/sistemi (test, deploy, ...)
- Diversi modi di lavorare (frontend, api/back, admin, mobile)
- Aziende "locali", si parla
- Meno centrale, meno ufficiale
