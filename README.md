# Questo è un repository navigabile

## E questo è il `README.md`

È solo un repository di prova costruito per testare alcune feature.


## Come si arriva qui?

Puoi arrivare qui dalla [esplorazione](https://gitlab.com/explore) dei repository oppure dalla [ricerca](https://gitlab.com/search?utf8=%E2%9C%93&search=appunti+gitlab&).

## Dove andare dopo?

Cerca e premi
**_[Avanti!](docs/browse.md)_**
per proseguire.


## Altro?

No, un `README` dovrebbe avere qualcosa dentro e questo cerca di simularlo.


## Novità

Ora con due [slide su Git](https://gitpitch.com/azienda/appunti-su-gitlab/master?grs=gitlab).


## Installazione

Non c'è nulla da installare. Potete comunque provare con:

```bash
npm run npm
```

